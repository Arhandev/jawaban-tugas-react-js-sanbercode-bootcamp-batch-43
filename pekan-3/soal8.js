const sum = (...params) => {
  let total = 0;
  for (let i = 0; i < params.length; i++) {
    total += params[i];
  }
  return total;
};

let result1 = sum(2, 4, 6); // akan menghasilkan 2 + 4 + 6
let result2 = sum(5, 2, 7, 4, 8); // akan menghasilkan 5 + 2 + 7 + 4 + 8

console.log(result1);
console.log(result2);
