const arr = [5, 3, 4, 7, 6, 9, 2];

const result = arr.map((item, index) => {
  if (index % 2 === 1) {
    return item * 3;
  } else {
    return item * 2;
  }
});

console.log(result);
